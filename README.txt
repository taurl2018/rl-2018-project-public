TAU RL DQN Project
By:
Guy Smoilovsky 305007916
Tomer Ben Moshe 303016661
Emma Rapoport 301481958

main.py was not changed much from original (apart from changing hyperparameters).
dqn_model.py has a couple of new models in addition to the ones provided.

dqn_learn.py was modified a bit from the original (except implementing the required bits of code), to allow saving and loading checkpoints. 
This was required for training on Google Colab, as it was impossible to get enough consecutive training time due to disconnections.

experiments.py is used as the entry point (main class) for all experiments beyond the scope of questions 1 and 2.
It activates either double_dqn_learn.py or dqn_learn_ex.py depending on command line arguments.
The two new dqn_learn classes are very simple to the original dqn_learn.py, but were changed to allow easier logging and checkpoints, and to allow double DQN.
Example of running experiments.py:

python experiments.py save_path=saved_state_double_dqn_separable_adam_qbert model=DQN_SEPARABLE learning_func=double_dqn optimizer=Adam gym_task_index=4