import sys


def get_cmd_kwargs(start_index=1):
    if len(sys.argv) < start_index:
        return {}
    else:
        d = {}
        for arg in sys.argv[start_index:]:
            k, v = arg.split("=")
            try:
                v = int(v)
            except ValueError:
                try:
                    v = float(v)
                except ValueError:
                    pass
            d[k] = v
        return d
