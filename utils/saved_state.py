import sys

class SavedState:
    def __init__(self, state_dict, timestep, stats):
        self.state_dict = state_dict
        self.stats = stats
        self.timestep = timestep

def try_to_find_saved_state():
    if len(sys.argv) >= 2:
        return sys.argv[1]
    else:
        return None